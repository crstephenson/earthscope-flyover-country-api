# earthscope-flyover-country-api

A standalone Express.js server that reads a collection of geolocated EarthScope
Science Nuggets from a GeoJSON file, accepts a WKT polygon from an HTTP GET
parameter, and serves the subset of Science Nuggets that intersect the polygon.

## Setup

These instructions are for CentOS 7, but a similar process should also work on
Ubuntu and other Linux distributions once Node.js is installed.

### Install system dependencies

```
$ curl -sL https://rpm.nodesource.com/setup_6.x | sudo bash -
$ sudo yum install nodejs
$ sudo npm install -g forever forever-service
```

### Create and enable the service

```
$ git clone https://crstephenson@bitbucket.org/crstephenson/earthscope-flyover-country-api.git
$ cd earthscope-flyover-country-api
$ npm install
$ sudo forever-service install earthscope-flyover-country-api --script index.js
$ sudo systemctl enable earthscope-flyover-country-api
$ sudo systemctl start earthscope-flyover-country-api
```

## Usage

With the `earthscope-flyover-country-api` service running, you should be able to
access the API on port 3000 of the server. For example:

http://example.com:3000/

If the script is working, this URL should return just `{}`.

You can get the set of Science Nuggets intersecting a region by passing a WKT
polygon as the `area` GET parameter, like this:

http://example.com:3000/?area=POLYGON((-180%20-90,%20180%20-90,%20180%2090,%20-180%2090,%20-180%20-90))

This WKT polygon represents the entire world, so it will return the full set of
Science Nuggets.
