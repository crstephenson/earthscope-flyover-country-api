/* jslint node: true, esversion: 6 */
'use strict';

require('console-stamp')(console, '[HH:MM:ss.l]');
const express = require('express');
const compression = require('compression');
const wellknown = require('wellknown');
const turf = require('@turf/turf');
const cors = require('cors');
const fs = require('fs');

const app = express();
app.use(compression());
app.use(cors());

let geojsonFile = 'sciencenuggets.json';

app.get('/', (expressReq, expressRes) => {
  if (!expressReq.query.area) {
    expressRes.json({});
    return;
  }

  let geometry;

  try {
    geometry = wellknown(expressReq.query.area);
  } catch (error) {
    expressRes.sendStatus(400).end();
    return;
  }

  if (!geometry) {
    expressRes.sendStatus(400).end();
    return;
  }

  let searchWithin = turf.feature(geometry);

  fs.readFile(geojsonFile, 'utf8', (err, geojsonContent) => {
    if (err) {
      console.error('Unable to read ' + geojsonFile);
      console.error(err);
    } else {
      let features = JSON.parse(geojsonContent);
      let intersecting = turf.pointsWithinPolygon(features, searchWithin);
      expressRes.json(intersecting);
    }
  });
});

app.listen(3000, () => console.log('Flyover Country API started.'));
